package no.uib.inf101.exam23v.tetromino;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;

/**
 * A tetromino is a shape that can be used in the game of Tetris. It 
 * consists of four quadratic blocks glued together along the edges.
 * <p>
 *   
 * This class is immutable.
 */
public final class Tetromino implements Iterable<GridCell<Character>> {
  
  private final char symbol;
  private final boolean[][] shape;
  private final CellPosition pos;

  /**
   * Creates a new tetromino with the given symbol. The shape of the
   * tetromino is determined by the symbol. The position of the tetromino
   * is set to (0, 0).
   *
   * @param sym the symbol of the tetromino
   *            (must be one of 'I', 'O', 'T', 'J', 'L', 'S', 'Z')
   * @throws IllegalArgumentException if the symbol is invalid
   * @return a new tetromino with the given symbol and corresponding shape
   *          and position (0, 0) (never null)
   */
  static Tetromino newTetromino(char sym) {
    boolean[][] shape = switch (sym) {
        case 'I' -> new boolean[][] {
            { false, false, false, false },
            {  true,  true,  true,  true },
            { false, false, false, false },
            { false, false, false, false },
        };
        case 'O' -> new boolean[][] {
            { false, false, false, false },
            { false,  true,  true, false },
            { false,  true,  true, false },
            { false, false, false, false },
        };
        case 'T' -> new boolean[][] {
            { false, false, false },
            {  true,  true,  true },
            { false,  true, false }
        };
        case 'J' -> new boolean[][] {
            { false, false, false },
            {  true,  true,  true },
            { false, false,  true }
        };
        case 'L' -> new boolean[][] {
            { false, false, false },
            {  true,  true,  true },
            {  true, false, false }
        };
        case 'S' -> new boolean[][] {
            { false, false, false },
            { false,  true,  true },
            {  true,  true, false }
        };
        case 'Z' -> new boolean[][] {
            { false, false, false },
            {  true,  true, false },
            { false,  true,  true }
        };
        default -> throw new IllegalArgumentException(
            "Unknown tetromino type: '" + sym + "'"
        );
    };
    return new Tetromino(sym, shape, new CellPosition(0, 0));
  }
  
  private Tetromino(char symbol, boolean[][] shape, CellPosition pos) {
    this.symbol = symbol;
    this.shape = shape;
    this.pos = pos;
  }
  
  /**
   * Gets a shifted copy of this tetromino. The returned tetromino will
   * have the same shape and symbol, but the position will be shifted
   * by the given deltaRow and deltaCol.
   *
   * @param deltaRow how much to shift the row
   * @param deltaCol how much to shift the column
   * @return a shifted copy of this tetromino
   */
  public Tetromino shiftedBy(int deltaRow, int deltaCol) {
    return new Tetromino(symbol, shape, new CellPosition(pos.row() + deltaRow, pos.col() + deltaCol));
  }
  
  @Override
  public Iterator<GridCell<Character>> iterator() {
    List<GridCell<Character>> list = new ArrayList<>();
    for (int i = 0; i < shape.length; i++) {
      for (int j = 0; j < shape[0].length; j++) {
        if (shape[i][j]) {
          CellPosition pos = new CellPosition(this.pos.row()+i, this.pos.col()+j);
          list.add(new GridCell<Character>(pos, symbol));
        }
      }
    }
    return list.iterator();
  }
  
  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;
    if (o instanceof Tetromino other) {
      return Objects.equals(this.symbol, other.symbol)
          && Arrays.deepEquals(shape, other.shape)
          && Objects.equals(this.pos, other.pos);
    } else {
      return false;
    }
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(
        this.symbol,
        Arrays.deepHashCode(this.shape),
        this.pos
    );
  }
}
